package com.magicapps.filemanager.common.navigation

import android.view.MenuItem
import androidx.fragment.app.FragmentContainerView
import androidx.navigation.NavController
import androidx.navigation.findNavController
import com.google.android.material.navigation.NavigationBarView
import com.magicapps.filemanager.R

class NavItemSelListener(private val navController: NavController) :
    NavigationBarView.OnItemSelectedListener {

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            R.id.images_item -> navController.navigate(R.id.imageFragment)
            R.id.audio_item -> navController.navigate(R.id.audioFragment)
            R.id.video_item -> navController.navigate(R.id.videoFragment)
            R.id.document_item -> navController.navigate(R.id.documentFragment)
            R.id.search_item -> navController.navigate(R.id.searchFragment)
        }
        return true
    }
}