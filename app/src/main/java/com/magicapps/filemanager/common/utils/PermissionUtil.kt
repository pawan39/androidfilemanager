package com.magicapps.filemanager.common.utils

import android.content.Context
import android.content.pm.PackageManager
import androidx.appcompat.app.AppCompatActivity

object PermissionUtil {
    sealed class Permission(val permissionName: String, val permissionCode: Int)

    class StoragePermission :
        Permission(android.Manifest.permission.READ_EXTERNAL_STORAGE, 1234)

    fun notHasPermission(context: Context, permission: Permission): Boolean {
        return context.checkSelfPermission(permission.permissionName) !=
                PackageManager.PERMISSION_GRANTED
    }

    fun requestPermission(activity:AppCompatActivity, permission: Permission){
        activity.requestPermissions(arrayOf(permission.permissionName), permission.permissionCode)
    }

    fun onActivityResult(activity: AppCompatActivity, requestCone:Int){
        if(requestCone == StoragePermission().permissionCode && notHasPermission(activity, StoragePermission())){
            requestPermission(activity, StoragePermission())
        }
        else{
            activity.recreate()
        }
    }
}