package com.magicapps.filemanager.common

import com.magicapps.filemanager.common.constants.FileUtils
import io.reactivex.rxjava3.core.Observable
import java.io.File

object FileCollector {
    private var mFiles: MutableList<File> = ArrayList()

    init {
        addFiles(FileUtils.getRootFile())
    }

    private fun addFiles(rootDirectory: File?) {
        if (rootDirectory != null) {
            if (rootDirectory.isDirectory &&
                rootDirectory.listFiles()!!.isNotEmpty() &&
                rootDirectory.listFiles() != null
            )
                rootDirectory.listFiles()!!.forEach(this::addFiles)
            if (rootDirectory.isFile) mFiles.add(rootDirectory)
        }
    }

    fun getFileObservable(): Observable<File> {
        return Observable.fromIterable(mFiles)
    }
}