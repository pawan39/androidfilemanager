package com.magicapps.filemanager.common.base

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewbinding.ViewBinding
import com.magicapps.filemanager.adapters.FileAdapter
import com.magicapps.filemanager.common.FileCollector
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Observer
import io.reactivex.rxjava3.disposables.Disposable
import io.reactivex.rxjava3.schedulers.Schedulers
import java.io.File

abstract class BaseFragment<Binding : ViewBinding> : Fragment() {

    var sExtension: String = ""
    private lateinit var fileAdapter: FileAdapter
    lateinit var fileRV: RecyclerView
    internal var mFiles: MutableList<File> = ArrayList()

    internal val extensionObsrvr = object : Observer<File> {

        private lateinit var disposable: Disposable
        override fun onSubscribe(d: Disposable) {
            this.disposable = d
        }

        override fun onNext(t: File) {
            mFiles.add(t)
        }

        override fun onError(e: Throwable) {
            e.printStackTrace()
        }

        @SuppressLint("NotifyDataSetChanged")
        override fun onComplete() {
            fileAdapter.setData(mFiles)
            fileAdapter.notifyDataSetChanged()
        }
    }

    override fun onStart() {
        super.onStart()
        FileCollector.getFileObservable()
            .filter { it.endsWith(this.sExtension) }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(extensionObsrvr)
    }

    abstract fun getBinding(container: ViewGroup?, inflater: LayoutInflater): Binding
    lateinit var binding: Binding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = getBinding(container, inflater)
        initViews()
        fileAdapter = FileAdapter(requireContext())
        fileRV.layoutManager = LinearLayoutManager(activity)
        fileRV.adapter = fileAdapter
        return binding.root
    }

    abstract fun initViews()
}