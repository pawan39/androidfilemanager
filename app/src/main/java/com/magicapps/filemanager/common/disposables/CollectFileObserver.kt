package com.magicapps.filemanager.common.disposables

import com.magicapps.filemanager.common.FileCollector
import io.reactivex.rxjava3.observers.DisposableObserver

class CollectFileObserver : DisposableObserver<Any>() {
    override fun onNext(t: Any) {}

    override fun onError(e: Throwable) {}

    //    We need onComplete only as Empty will only hit this one.
    override fun onComplete() {
        FileCollector
    }
}