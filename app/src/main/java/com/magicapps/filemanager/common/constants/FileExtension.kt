package com.magicapps.filemanager.common.constants

sealed class FileExtension(val type: String) {
    inner class Image : FileExtension("jpg")
    inner class Video : FileExtension("mp4")
    inner class Document : FileExtension("pdf")
    inner class Audio : FileExtension("mp3")
}
