package com.magicapps.filemanager.common.navigation

import android.os.Bundle
import androidx.navigation.NavController
import androidx.navigation.NavDestination
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.magicapps.filemanager.R

class NavDestinationChangeListener(private val bottomNavigationView: BottomNavigationView) :
    NavController.OnDestinationChangedListener {
    override fun onDestinationChanged(
        controller: NavController,
        destination: NavDestination,
        arguments: Bundle?
    ) {
        val menu = bottomNavigationView.menu
        when (controller.currentDestination!!.id) {
            R.id.imageFragment -> menu.findItem(R.id.images_item).isChecked = true
            R.id.audioFragment -> menu.findItem(R.id.audio_item).isChecked = true
            R.id.videoFragment -> menu.findItem(R.id.video_item).isChecked = true
            R.id.documentFragment -> menu.findItem(R.id.document_item).isChecked = true
            R.id.searchFragment -> menu.findItem(R.id.search_item).isChecked = true
        }
    }
}