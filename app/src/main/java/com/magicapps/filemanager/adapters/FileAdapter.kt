package com.magicapps.filemanager.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.magicapps.filemanager.R
import com.magicapps.filemanager.common.constants.FileUtils
import java.io.File

class FileAdapter(private val context: Context) :
    RecyclerView.Adapter<FileAdapter.FilesViewHolder>() {
    private var data: MutableList<File> = ArrayList()

    inner class FilesViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val title: TextView = itemView.findViewById(R.id.fileNameTv)
        val size: TextView = itemView.findViewById(R.id.fileSizeTv)
        val modified: TextView = itemView.findViewById(R.id.lastModifiedTv)
    }

    fun setData(data: MutableList<File>) {
        this.data = data
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FilesViewHolder {
        return FilesViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.file_item, parent, false)
        )
    }

    override fun onBindViewHolder(holder: FilesViewHolder, position: Int) {
        holder.title.text = FileUtils.getName(data[position])
        holder.modified.text = FileUtils.getLastModifiedDate(data[position])
        holder.size.text = FileUtils.getSize(context, data[position])
    }

    override fun getItemCount(): Int {
        return data.size
    }
}