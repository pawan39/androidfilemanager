package com.magicapps.filemanager.common.constants

import android.content.Context
import android.os.Environment
import android.text.format.Formatter.formatFileSize
import java.io.File
import java.text.SimpleDateFormat
import java.util.*

object FileUtils {
    private const val britishDateFormat = "dd/MM/yyyy"
    fun getName(file: File?) = file?.name.toString() ?: ""

    fun getSize(context: Context, file: File?): String =
        formatFileSize(context, file?.length() ?: 0L)

    fun getLastModifiedDate(file: File?): String =
        SimpleDateFormat(britishDateFormat, Locale.ENGLISH).format(file?.lastModified() ?: 0L)

    fun getRootFile(): File {
        val path = Environment.getExternalStorageDirectory().path
        return File(path)
    }
}