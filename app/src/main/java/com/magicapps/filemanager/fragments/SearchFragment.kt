package com.magicapps.filemanager.fragments

import android.view.LayoutInflater
import android.view.ViewGroup
import com.google.android.material.button.MaterialButton
import com.google.android.material.textfield.TextInputEditText
import com.magicapps.filemanager.R
import com.magicapps.filemanager.common.FileCollector
import com.magicapps.filemanager.common.base.BaseFragment
import com.magicapps.filemanager.databinding.FragmentSearchBinding
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.schedulers.Schedulers

class SearchFragment : BaseFragment<FragmentSearchBinding>() {
    private lateinit var sExtensionIET: TextInputEditText
    private lateinit var searchBtn: MaterialButton

    override fun getBinding(
        container: ViewGroup?,
        inflater: LayoutInflater
    ): FragmentSearchBinding {
        return FragmentSearchBinding.inflate(inflater, container, false)
    }

    override fun initViews() {
        fileRV = binding.searchRv
        sExtension = " "
    }

    override fun onStart() {
        super.onStart()

        sExtensionIET = requireView().findViewById(R.id.ITSearch)
        searchBtn = requireView().findViewById(R.id.btnSearch)

        searchBtn.setOnClickListener {
            mFiles = ArrayList()
            sExtension = sExtensionIET.text.toString()
            FileCollector.getFileObservable()
                .filter {
                    it.name.contains(this.sExtension)
                }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(extensionObsrvr)
        }
    }
}