package com.magicapps.filemanager.activities

import android.content.Intent
import com.magicapps.filemanager.common.base.BaseActivity
import com.magicapps.filemanager.common.disposables.CollectFileObserver
import com.magicapps.filemanager.common.utils.PermissionUtil
import com.magicapps.filemanager.databinding.ActivitySplashScreenBinding
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.schedulers.Schedulers

class SplashScreenActivity : BaseActivity<ActivitySplashScreenBinding>() {
    override fun getViewBinding(): ActivitySplashScreenBinding {
        return ActivitySplashScreenBinding.inflate(layoutInflater)
    }

    override fun onStart() {
        super.onStart()

        if (PermissionUtil.notHasPermission(this, PermissionUtil.StoragePermission())) {
            PermissionUtil.requestPermission(this, PermissionUtil.StoragePermission())
            return
        }

        Observable.empty<String>()
            .subscribeOn(Schedulers.io())
            .subscribe(CollectFileObserver())

        startActivity(Intent(this, MainActivity::class.java))
        this.finish()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        PermissionUtil.onActivityResult(this, requestCode)
    }

    override fun initViews() {}
}