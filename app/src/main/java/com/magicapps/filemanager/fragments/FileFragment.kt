package com.magicapps.filemanager.fragments

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.fragment.navArgs
import com.magicapps.filemanager.common.base.BaseFragment
import com.magicapps.filemanager.databinding.FragmentFileBinding


class FileFragment : BaseFragment<FragmentFileBinding>() {
    private val args: FileFragmentArgs by navArgs()
    override fun getBinding(container: ViewGroup?, inflater: LayoutInflater): FragmentFileBinding {
        return FragmentFileBinding.inflate(inflater, container, false)
    }

    override fun initViews() {
        fileRV = binding.fileRecyclerView
        this.sExtension = args.fileExtension.toString()
    }
}