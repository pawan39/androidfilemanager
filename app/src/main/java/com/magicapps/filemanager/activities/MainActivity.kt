package com.magicapps.filemanager.activities

import androidx.fragment.app.FragmentContainerView
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.magicapps.filemanager.R
import com.magicapps.filemanager.common.base.BaseActivity
import com.magicapps.filemanager.common.navigation.NavDestinationChangeListener
import com.magicapps.filemanager.common.navigation.NavItemSelListener
import com.magicapps.filemanager.databinding.ActivityMainBinding

class MainActivity : BaseActivity<ActivityMainBinding>() {
    private lateinit var mainBotNav: BottomNavigationView
    private lateinit var mainFCV: FragmentContainerView

    override fun getViewBinding(): ActivityMainBinding {
        return ActivityMainBinding.inflate(layoutInflater)
    }

    override fun initViews() {
        val mainNHF = supportFragmentManager
            .findFragmentById(R.id.fcv_main) as NavHostFragment

        mainBotNav = binding.mainBotNavBar
        mainFCV = binding.mainFcv

        val navCont = mainNHF.navController
        mainBotNav.setupWithNavController(navCont)
        navCont.addOnDestinationChangedListener(NavDestinationChangeListener(mainBotNav))
        mainBotNav.setOnItemSelectedListener(NavItemSelListener(navCont))
    }

    override fun onBackPressed() {
        this.finish()
    }
}